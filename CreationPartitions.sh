#!/bin/bash
#---------------------------------------------------
# Title...........: creation_partitions.sh
# Created.........: mar. 10 sept. 2024 22:11:03 CEST
# Author..........: dp403aan
# Usage...........:
# Notes...........:
#---------------------------------------------------
rm creation_*_partitions.csv
SEQ=('1 2 3' '2 3 4' '3 4 5' '4 5 1' '5 1 2')

read -p "- Date de MEP du fichier (JJMMAAAA) ? " DATEFIC
FIC="creation_${DATEFIC}_partitions.csv"
read -p "- Combien de partitions de taille S à renseigner ? " NBPARTITIONS

echo "service taille numTbsB numTbsM numTbsS" > "${FIC}"
read -p "- Dernière séquence de 3 chiffres du dernier fichier joué (ex : 2 3 4) ? " SEQS

for i in "${!SEQ[@]}"; do
  if [[ "${SEQS}" == "${SEQ[$i]}" ]]; then
    INDEX=$i
    break
  fi
done

for ((NUMBER = 1; NUMBER <= NBPARTITIONS; NUMBER++)); do
  read -p "Service #${NUMBER} (Taille S) ? " SERVICE
  # Calcul de l'indice de la séquence suivante
  NEXT_INDEX=$(( (INDEX + NUMBER) % ${#SEQ[@]} ))
  echo "${SERVICE} S ${SEQ[NEXT_INDEX]}" >> "${FIC}"
done

printf "\n- Combien de partitions de taille M à renseigner ? " && read -r NBPARTITIONS
read -p "- Dernière séquence de 3 chiffres du dernier fichier joué (ex : 3 4 5) ? " SEQM

for i in "${!SEQ[@]}"; do
  if [[ "${SEQM}" == "${SEQ[$i]}" ]]; then
    INDEX=$i
    break
  fi
done

for ((NUMBER = 1; NUMBER <= NBPARTITIONS; NUMBER++)); do
  read -p "Service #${NUMBER} (Taille M) ? " SERVICE
  # Calcul de l'indice de la séquence suivante
  NEXT_INDEX=$(( (INDEX + NUMBER) % ${#SEQ[@]} ))
  echo "${SERVICE} M ${SEQ[NEXT_INDEX]}" >> "${FIC}"
done
echo -e "\n- Le fichier de partitions ${FIC} a été créé, en voici son contenu :\n"
cat "${FIC}"
