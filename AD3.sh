#!/bin/bash
#---------------------------------------------------
# Title...........: AD3.sh
# Created.........:
# Author..........: dp403aan
# Description.....:
# Usage...........:
# Notes...........:
#---------------------------------------------------
while getopts "t:d:f:" option; do
  case $option in
    t)
      type="$OPTARG"
      ;;
    d)
      start_date="$OPTARG"
      ;;
    f)
      end_date="$OPTARG"
      ;;
    \?)
      echo "Option invalide: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

shift $((OPTIND-1))

[[ -z "$type" ]] || [[ -z "$start_date" ]] || [[ -z "$end_date" ]] && { echo "Usage: $0 -t <bds|cdd|cdt> -d DDMMYYYY -f DDMMYYYY file_to_rename.pdf" ; exit 1; }

file_to_rename="$1"

case "$type" in
  "bds")
    new_name="Bulletin.de.salaire.du.$start_date.au.$end_date.pdf"
    ;;
  "cdd")
    new_name="CDD.du.$start_date.au.$end_date.pdf"
    ;;
  "cdt")
    new_name="Certificat.de.travail.du.$start_date.au.$end_date.pdf"
    ;;
  *)
    echo "Les types valides sont : bds, cdd, cdt."
    exit 1
    ;;
esac

mv "$file_to_rename" "$new_name"
