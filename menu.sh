#!/bin/bash
#---------------------------------------------------
# Title...........: menu.sh
# Created.........:
# Author..........:
# Description.....:
# Usage...........:
# Notes...........:
#---------------------------------------------------
clear

function projectOne_conn()
{
 echo "Connecting to projectOne.."
 echo -e "- PTF (preprod | qual | prod) ?"
 read PTF
 echo -e "- LAYER ?"
 read LAYER
 cd /home/${USER}/Git/projectOne/forge/scripts
 clear
./run.sh frgSsh.py projectOne-${PTF} --a projectOne --la ${LAYER} --bastionUsername user --bastionUserKey /home/${USER}/.ssh/user.pem --instanceUsername debian --instanceUserKey /home/${USER}/.ssh/id_rsa_projectOne_${PTF} --lo /home/${USER}/Git/projectOne/projectOne-provisionning --instanceIndex 0 --m tty
}

function projectOne_pbk()
{
 echo "Playbook for projectOne"
 echo -e "- PTF (preprod | qual | prod) ?"
 read PTF
 echo -e "- PLAYBOOK NAME ?"
 read PLAYBOOKNAME
 cd /home/${USER}/Git/projectOne/forge/scripts
 clear
 ./run.sh frgAnsiblePlaybook projectOne-${PTF} --appli projectOne --bastionUsername user --bastionUserKey /home/${USER}/.ssh/user.pem --instanceUsername debian --instanceUserKey /home/${USER}/.ssh/id_rsa_projectOne_${PTF} --playbook ${PLAYBOOKNAME} --externalVarsFile /home/${USER}/Git/projectOne/projectOne-provisionning/playbooks/external_vars/projectOne-${PTF}.yml --localGitPath /home/${USER}/Git/projectOne/projectOne-provisionning
}

function projectOne_fi()
{
 echo "Forging image for projectOne.."
 echo -e "- PTF (preprod | qual | prod) ?"
 read PTF
 echo -e "- LAYER ?"
 read LAYER
 cd /home/${USER}/Git/projectOne/forge/scripts
 clear
 ./run.sh frgForgeImage projectOne-${PTF} --appli projectOne --layer ${LAYER} --externalVarsFile /home/${USER}/Git/projectOne/projectOne-provisionning/playbooks/external_vars/projectOne-${PTF}.yml --instancePublicKey /home/${USER}/.ssh/user.pem.pub --localGitPath /home/${USER}/Git/projectOne/projectOne-provisionning --delete
}

function projectOne_dbg()
{
 echo "doBlueGreening projectOne.."
 echo -e "- PTF (preprod | qual | prod) ?"
 read PTF
 echo -e "- LAYER ?"
 read LAYER
 cd /home/${USER}/Git/projectOne/forge/scripts
 clear
 ./run.sh frgDoBlueGreen.py projectOne-${PTF} --appli projectOne --layer ${LAYER} --localGitPath /home/${USER}/Git/projectOne/projectOne-provisionning --force
}

function projectTwo_conn()
{
 echo "Connecting to projectTwo.."
 echo -e "- PTF (int | qual | prod) ?"
 read PTF
 echo -e "- LAYER ?"
 read LAYER
 cd /home/${USER}/Git/projectTwo/forge/scripts
 clear
 ./run.sh frgSsh.py projectTwo_${PTF} --a projectTwo --la ${LAYER} --instanceUsername debian --instanceUserKey /home/${USER}/.ssh/id_rsa_projectTwo_${PTF} --bastionUsername user --bastionUserKey /home/${USER}/.ssh/user.pem --lo /home/${USER}/Git/projectTwo/projectTwo-provisioning/ --instanceIndex 0 --m tty
}

function projectTwo_pbk()
{
 echo "Playbook for projectTwo"
 echo -e "- PTF (int | qual | prod) ?"
 read PTF
 echo -e "- PLAYBOOK NAME ?"
 read PLAYBOOKNAME
 cd /home/${USER}/Git/projectTwo/forge/scripts
 clear
 ./run.sh frgAnsiblePlaybook projectTwo_${PTF} --appli projectTwo --bastionUsername user --bastionUserKey /home/${USER}/.ssh/user.pem --instanceUsername debian --instanceUserKey /home/${USER}/.ssh/id_rsa_projectTwo_${PTF} --playbook ${PLAYBOOKNAME} --externalVarsFile /home/${USER}/Git/projectTwo/projectTwo-provisioning/playbooks/extra_vars/extra_vars_file_projectTwo_${PTF} --localGitPath /home/${USER}/Git/projectTwo/projectTwo-provisioning --vaultid /home/${USER}/Git/projectTwo/projectTwo-provisioning/playbooks/vault_files/projectTwo_vault_password
}

function projectTwo_fi()
{
 echo "Forging image for projectTwo.."
 echo -e "- PTF (int | qual | prod) ?"
 read PTF
 echo -e "- LAYER ?"
 read LAYER
 cd /home/${USER}/Git/projectTwo/forge/scripts
 clear
 ./run.sh frgForgeImage projectTwo_${PTF} --appli projectTwo --layer ${LAYER} --externalVarsFile /home/${USER}/Git/projectTwo/projectTwo-provisioning/playbooks/extra_vars/extra_vars_file_projectTwo_${PTF} --instancePublicKey /home/${USER}/.ssh/id_rsa_projectTwo_${PTF} --localGitPath /home/${USER}/Git/projectTwo/projectTwo-provisioning --vaultid /home/${USER}/Git/projectTwo/projectTwo-provisioning/playbooks/vault_files/projectTwo_vault_password --imageBaseName debian9-mi-forgedc-2021.25 --delete
}

function projectTwo_dbg()
{
 echo "doBlueGreening projectTwo.."
 echo -e "- PTF (int | qual | prod) ?"
 read PTF
 echo -e "- LAYER ?"
 read LAYER
 cd /home/${USER}/Git/projectTwo/forge/scripts
 clear
 ./run.sh frgDoBlueGreen projectTwo_${PTF} --appli projectTwo --layer ${LAYER} --localGitPath /home/${USER}/Git/projectTwo/projectTwo-provisioning --force
}

menu()
{
 echo -ne "
---------------------------------
        SAMPLE SHELL MENU
[projectOne 1-4 | projectTwo 5-8]
---------------------------------

 1) Connection
 2) Playbook
 3) ForgeImage
 4) doBlueGreen
 --
 5) Connection
 6) Playbook
 7) ForgeImage
 8) doBlueGreen
 --
 0) Quit

 Choice ? "
 read REP
 case "${REP}" in
     1) projectOne_conn ; clear ; menu ;;
     2) projectOne_pbk ; clear ; menu ;;
     3) projectOne_fi ; clear ; menu ;;
     4) projectOne_dbg ; clear ; menu ;;
     5) projectTwo_conn ; clear ; menu ;;
     6) projectTwo_pbk ; clear ; menu ;;
     7) projectTwo_fi ; clear ; menu ;;
     8) projectTwo_dbg ; clear ; menu ;;
     0) exit 0 ;;
     *) clear ; echo -e "\n!! ${REP} is an invalid option !!" ; menu ;;
 esac
}

# Run the menu
menu

