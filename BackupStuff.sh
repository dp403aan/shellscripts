#!/bin/bash

TIMESTAMP=$(date +%d%m%Y.%H%M%S)
BACKUPDIR="${HOME}/IT/LAB/BACKUPS"
CONFIG="${HOME}/.config"
ETC="/etc"

STUFF=(
  "${HOME}/.vimrc"
  "${HOME}/.bashrc"
  "${HOME}/.gitconfig"
  "${HOME}/.git-credentials"
  "${HOME}/.Xresources"
  "${CONFIG}/mpv/mpv.conf"
  "${HOME}/.vim/misc"
  "${HOME}/.ssh"
  "${CONFIG}/i3"
  "${CONFIG}/i3status"
  "${CONFIG}/ranger"
  "${CONFIG}/khal"
  "${CONFIG}/cmus"
  "${ETC}/apt"
  "${ETC}/wpa_supplicant"
)

tar czf ${BACKUPDIR}/BACKUP-${TIMESTAMP}.tar.gz "${STUFF[@]}"

ls -lh ${BACKUPDIR}
