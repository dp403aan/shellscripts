#!/bin/bash
#---------------------------------------------------
# Title...........: MusicStuff.sh
# Created.........:
# Author..........: dp403aan
# Description.....:
# Usage...........:
# Notes...........:
#---------------------------------------------------
SOURCEDIR="/home/${USER}/Music/SRC/"
DESTDIR="/home/${USER}/Music/DEST/"
LOGDIR="/home/${USER}/Music/LOGS/"
DATE=`date '+%Y%m%d_%H%M%S'`
PROGNAME="$(basename "$0" | cut -d . -f1)"
FLOG=${LOGDIR}${PROGNAME}_${DATE}.log
LISTARCHIVES="${SOURCEDIR}/[0-9].*"

exec &> ${FLOG}

ext()
{
 if [ -f $1 ]
 then
  case $1 in
   *.rar)   unar $1 -o ${DESTDIR}  ;;
   *.zip)   unzip $1 -d ${DESTDIR} ;;
   *.7z)    7z x $1 -o${DESTDIR}   ;;
   *)       echo "'$1' cannot be extracted" ;;
  esac
 else
  echo "'$1' is not a valid file"
 fi
}

# Step 1 : Decompressing archives
for ARCHIVE in ${LISTARCHIVES}; do
  ext "${ARCHIVE}"
  rm "${ARCHIVE}"
done

# Step 2 : Removing useless files, folders and release year
UNWANTEDFILES=("Albumart*" "*.ini" "*.url" "*.m3u*" "*.pdf" "*.txt")

for FILE in "${UNWANTEDFILES[@]}"; do
  find "${DESTDIR}" -type f -name "${FILE}" -exec rm -fv {} +
done

DIRSTODELETE=("*[Ss]can*" "*[Cc]overs*" "*[Aa]rt*")

for DIR in "${DIRSTODELETE[@]}"; do
  find "${DESTDIR}" -type d -name "${DIR}" -exec rm -fr {} +
done

for FOLDERNAME in ${DESTDIR}/* ; do mv "${FOLDERNAME}" "$(echo "${FOLDERNAME}" | sed -e 's/[0-9]\{4\} //' -e 's/ (EP)//')" ; done

# Step 3 : Renaming to Cover.jpg
TOBERENAMED=("[Ff]older.jpg"  "[Ff]older.jpeg" "cover.jpg" "[Cc]over.jpeg" "*[Ff]ront*")

for FILE in "${TOBERENAMED[@]}"; do
  find "${DESTDIR}" -type f -name "${FILE}" -exec sh -c 'mv "$1" "${1%/*}/Cover.jpg"' sh {} \;
done

find "${DESTDIR}" -type f -name "Cover.png" -exec sh -c 'convert "$1" "${1%.png}.jpg"' sh {} \;

# Step 4 : Resizing Cover.jpg to 300x300 px
for FOLDER in ${DESTDIR}/* ; do
  convert "${FOLDER}/Cover.jpg" -resize 300x300 "${FOLDER}/300.jpg"; mv "${FOLDER}/300.jpg" "${FOLDER}/Cover.jpg" ;
done

