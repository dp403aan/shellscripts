# Shell scripts

Some useful shell scripts

* **menu.sh**: A basic menu written in bash
* **RetrieveLogs.ksh**: A ksh shellscript to retrieve logs and send them by email
* **MusicStuff.sh**: A bash script for renaming music folders
* **AD3.sh**: A bash script to rename files with options (getopts)
* **BackupStuff.sh**: A basic backup script
* **CreationPartitions.sh**: A bash script to generate a file to create sequences
